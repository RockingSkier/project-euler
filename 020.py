"""n! means n  (n  1)  ...  3  2  1

For example, 10! = 10  9  ...  3  2  1 = 3628800,
and the sum of the digits in the number 10! is 3 + 6 + 2 + 8 + 8 + 0 + 0 = 27.

Find the sum of the digits in the number 100!

"""

sum = 100
num = sum - 1
digit_sum = 0

while (num > 1):
    sum *= num
    num -= 1

print('Sum: %d' % sum)

for char in str(sum):
    digit_sum += int(char)

print('Digit sum: %d' % digit_sum)