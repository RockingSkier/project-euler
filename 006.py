"""The sum of the squares of the first ten natural numbers is,

12 + 22 + ... + 102 = 385
The square of the sum of the first ten natural numbers is,

(1 + 2 + ... + 10)2 = 552 = 3025
Hence the difference between the sum of the squares of the first ten natural numbers and the square of the sum is 3025  385 = 2640.

Find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum.

"""

sum_sqr, sqr_sum = 0, 0

for num in range(0,100):
    num += 1
    # Sum  of the squares
    sum_sqr += num ** 2

    # Square of sum
    sqr_sum += num

sqr_sum **= 2

difference = sqr_sum - sum_sqr

print(u'Sum of squares: %(sum_sqr)d' % {'sum_sqr': sum_sqr})
print(u'Square of sum: %(sqr_sum)d' % {'sqr_sum': sqr_sum})
print(u'Difference: %(difference)d' % {'difference': difference})
